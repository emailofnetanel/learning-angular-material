class UserDetailsController  {

  /**
   * Constructor
   *
   * @param $mdDialog
   * @param $log
   */
  constructor($mdDialog, $log) {
    this.$mdDialog = $mdDialog;
    this.$log = $log;
  }


  /**
   * Show the bottom sheet
   */
  share(ev) {
    this.$mdDialog.show(
        // this.$mdDialog.alert()
        //     .parent(angular.element(document.querySelector('#popupContainer')))
        //     .clickOutsideToClose(true)
        //     .title('This is an alert title')
        //     .textContent('You can specify some description text in here.')
        //     .ariaLabel('Alert Dialog Demo')
        //     .ok('Got it!')
        //     .targetEvent(ev)
        {

          templateUrl: 'user-form.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose: true
        }
    );
  }
}
export default UserDetailsController;

